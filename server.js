var matches = {};
var players = {};
var playersQueue = [];
var playersOnline = 0;
var balancingData = {};

var isAdmin = false;

fs = require('fs')
fs.readFile('data.json', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  balancingData = JSON.parse(data);
});

var express = require('express');
var app = express();

app.use(require('body-parser').json());

app.get('/', function(req, res){
   res.sendFile(__dirname + '/public/index.html');
});

app.get('/data', function(req, res){
	res.end(JSON.stringify(balancingData));
});

app.post('/data', function(req, res){
	if (!isAdmin) {
		res.status(403).end();
		return;
	}
	balancingData = req.body;
	
	fs.writeFile('data.json', JSON.stringify(req.body), (err) => {
		var status;
  		if (err) {
  			status = 403;
  			throw err;
  		} else {
  			status = 200;
  		}
  		isAdmin = false;
  		res.status(status).end();
	});
});

app.post('/login', function(req, res){
	if (req.body.name == "deardeer" && req.body.password == "T2.%PXbR") {
		isAdmin = true;
		res.status(200).end();
	} else {
		res.status(403).end();
	}
});

app.use(express.static('public'));

app.listen(3000);

const io = require('socket.io')(8080);

io.on('connection', function(socket) {
	playersOnline++;
	createNewPlayer(socket);

	socket.on('findMatch', function() {
		if (playersQueue.length > 0) {
			var matchInfo = new MatchInfo();
			matchInfo.identifier = getMatchIdentifier();

			matchInfo.playerOne = playersQueue.shift();
			matchInfo.playerTwo = players[socket.id];

			matchInfo.playerOne.isInQueue = false;
			matchInfo.playerTwo.isInQueue = false;

			matchInfo.playerOne.currentMatch = matchInfo.identifier;
			matchInfo.playerTwo.currentMatch = matchInfo.identifier;

			matches[matchInfo.identifier] = matchInfo;

			matchInfo.playerOne.socket.send(getMatchData(matchInfo, true));
			matchInfo.playerTwo.socket.send(getMatchData(matchInfo, false));
		} else {
			var player = players[socket.id];
			player.isInQueue = true;
			playersQueue.push(player);
		}
	});

	socket.on('disconnect', function() {
		var obj = {
			msgType : "playersOnline",
			msgObject : --playersOnline
		}
		io.send(obj);

		var player = players[socket.id];

		if (player != null) {
			if (player.isInQueue) {
				for (var i = 0; i < playersQueue.length; i++) {
					if (playersQueue[i].socket.id == socket.id) {
						playersQueue.splice(i, 1);
						break;
					}
				}
			}

			if (player.currentMatch != null) {
				var matchInfo = matches[player.currentMatch];

				if (matchInfo != null) {
					if (matchInfo.playerOne != null && matchInfo.playerOne.socket.id == socket.id) {
						matchInfo.playerOne = null;
					}

					if (matchInfo.playerTwo != null && matchInfo.playerTwo.socket.id == socket.id) {
						matchInfo.playerTwo = null;
					}

					if (matchInfo.playerOne == null && matchInfo.playerTwo == null) {
						delete matches[matchInfo.identifier];
					}
				}
			}

			delete players[socket.id];
		}
	});

	socket.on('message', function(data) {
		var matchInfo = matches[data.identifier];
		if (matchInfo != null && matchInfo.playerOne != null && matchInfo.playerTwo != null) {
			if (matchInfo.playerOne.socket.id == socket.id) {
				matchInfo.playerTwo.socket.send(combineTypeAndMessage(data.msgType, data.content, data.id));
			}

			if (matchInfo.playerTwo.socket.id == socket.id) {
				matchInfo.playerOne.socket.send(combineTypeAndMessage(data.msgType, data.content, data.id));
			}
		} else if (matchInfo == null) {
			socket.send({msgType : "matchDeleted", msgObject : null});
			players[socket.id].currentMatch = null;
		}
	});

	socket.on('cancelSearch', function() {
		for (var i = 0; i < playersQueue.length; i++) {
			if (playersQueue[i].socket.id == socket.id) {
				var player = players[socket.id];
				if (player != null) {
					player.isInQueue = false;
					console.log('canceled search');
				}
				playersQueue.splice(i, 1);
				break;
			}
		}
	});

	socket.on('matchDeleted', function(matchIdentifier) {
		var matchInfo = matches[matchIdentifier];

		if (matchInfo != null) {
			delete matches[matchIdentifier];
		}

		players[socket.id].currentMatch = null;
	});

	socket.on('balancingData', function() {
		var msg = {
			msgType : "balancingData",
			msgObject : balancingData
		}
		socket.send(msg);
	});

	socket.on('playersOnline', function() {
		var obj = {
			msgType : "playersOnline",
			msgObject : playersOnline
		}
		io.send(obj);
	});

	socket.on('addPlayer', function(matchIdentifier) {
		var player = players[socket.id];

		var responseCode = 400;
		if (player != null) {
			var match = matches[matchIdentifier];

			if (match != null) {
				player.currentMatch = matchIdentifier;
				if (match.playerOne == null) {
					match.playerOne = player;
					responseCode = 200;
				} else if (match.playerTwo == null) {
					match.playerTwo = player;
					responseCode = 200;
				}
			} else {
				responseCode = 300;
			}
		}

		socket.send({msgType : "addPlayer", msgObject : {response: responseCode}});
	});
});

function getMatchIdentifier() {
  return Math.random().toString(36).substr(2, 16);
}

function getMatchData(matchInfo, host) {
	var obj = {
		msgType : "matchData",
		msgObject : {
			matchIdentifier : matchInfo.identifier,
			isHost : host
		}
	}

	return obj;
}

function combineTypeAndMessage(messageType, msg, msgID) {
	return {
		msgType : messageType,
		msgObject : msg,
		id : msgID
	};
}

function createNewPlayer(socket) {
	var player = new Player();
	player.isInQueue = false;
	player.socket = socket;
	players[socket.id] = player;
}

function MatchInfo() {
	var identifier;
	var playerOne;
	var playerTwo;
}

function Player() {
	var isInQueue;
	var currentMatch;
	var socket;
}