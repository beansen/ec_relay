var url = "http://ec2-18-194-73-149.eu-central-1.compute.amazonaws.com";
var content;

window.onload = function() {
	content = document.getElementById('content');
	showLogin();
};

function showLogin() {
	while (content.firstChild) {
    	content.removeChild(content.firstChild);
	}

	var table = document.createElement("TABLE");
	var row = table.insertRow(0);
	var nameCell = row.insertCell(0);
	nameCell.style.width = "100px";
	nameCell.innerHTML = "Name";
	var nameInputCell = row.insertCell(1);
	var nameInput = document.createElement("INPUT");
	nameInput.setAttribute("type", "text");
	nameInputCell.appendChild(nameInput);

	row = table.insertRow(1);
	var passwordCell = row.insertCell(0);
	passwordCell.style.width = "100px";
	passwordCell.innerHTML = "Password";
	var passwordInputCell = row.insertCell(1);
	var passwordInput = document.createElement("INPUT");
	passwordInput.setAttribute("type", "password");
	passwordInputCell.appendChild(passwordInput);

	row = table.insertRow(2);
	row.insertCell(0);
	var buttonCell = row.insertCell(1);
	var button = document.createElement("BUTTON");
	button.style.width = "100px";
	button.type = "submit";
	button.innerHTML = "Login";
	button.onclick = function() {
		var params = JSON.stringify({name:nameInput.value, password:passwordInput.value});
		var http = new XMLHttpRequest();
		http.open("POST", url + '/login', true);
		http.onreadystatechange = function() {
			checkLogin(this.readyState, this.status);
		};
		http.setRequestHeader("Content-type", "application/json");
		http.send(params);
	};
	buttonCell.appendChild(button);
	
	content.appendChild(table);
}

function checkLogin(readyState, status) {
	if (readyState == 4) {
		if (status == 200) {
			var http = new XMLHttpRequest();
		http.open("GET", url + '/data', true);
		http.onreadystatechange = function() {
			if (this.readyState == 4 && status == 200) {
				showBalancingData(http.response);
			}
		};
		http.send(null);
		} else {
			alert("Login failed");
		}
	}
}

function showBalancingData(balancingData) {
	while (content.firstChild) {
    	content.removeChild(content.firstChild);
	}

	var fishDataTable = document.createElement("TABLE");
	fishDataTable.style.border = "thin solid #000000";
	fishDataTable.rules = "all";
	var row = fishDataTable.insertRow(0);
	var cell = row.insertCell(0);
	cell.style.width = "100px";
	cell.innerHTML = "ID";
	cell.style.textAlign = "center";
	cell = row.insertCell(1);
	cell.style.width = "150px";
	cell.innerHTML = "Name";
	cell.style.textAlign = "center";
	cell = row.insertCell(2);
	cell.style.width = "100px";
	cell.innerHTML = "Amount";
	cell.style.textAlign = "center";

	var data = JSON.parse(balancingData);
	for (var i = 0; i < data.fishData.length; i++) {
		var rowIndex = i + 1;
		var fishData = data.fishData[i];
		row = fishDataTable.insertRow(rowIndex);
		var cell = row.insertCell(0);
		cell.style.width = "100px";
		cell.innerHTML = fishData.id;
		cell.style.textAlign = "center";
		cell = row.insertCell(1);
		cell.style.width = "150px";
		cell.innerHTML = fishData.name;
		cell.style.textAlign = "center";
		cell = row.insertCell(2);
		cell.style.width = "100px";
		var amountInput = document.createElement("INPUT");
		amountInput.setAttribute("type", "text");
		amountInput.value = fishData.amount;
		amountInput.style.textAlign = "center";
		cell.appendChild(amountInput);
	}

	var playerCardsTable = document.createElement("TABLE");
	playerCardsTable.style.border = "thin solid #000000";
	playerCardsTable.rules = "all";
	var row = playerCardsTable.insertRow(0);
	var cell = row.insertCell(0);
	cell.style.width = "100px";
	cell.innerHTML = "ID";
	cell.style.textAlign = "center";
	cell = row.insertCell(1);
	cell.style.width = "150px";
	cell.innerHTML = "Name";
	cell.style.textAlign = "center";
	cell = row.insertCell(2);
	cell.style.width = "100px";
	cell.innerHTML = "Amount";
	cell.style.textAlign = "center";

	for (var i = 0; i < data.playerData.length; i++) {
		var rowIndex = i + 1;
		var playerData = data.playerData[i];
		row = playerCardsTable.insertRow(rowIndex);
		var cell = row.insertCell(0);
		cell.style.width = "100px";
		cell.innerHTML = playerData.id;
		cell.style.textAlign = "center";
		cell = row.insertCell(1);
		cell.style.width = "150px";
		cell.innerHTML = playerData.name;
		cell.style.textAlign = "center";
		cell = row.insertCell(2);
		cell.style.width = "100px";
		var amountInput = document.createElement("INPUT");
		amountInput.setAttribute("type", "text");
		amountInput.value = playerData.amount;
		amountInput.style.textAlign = "center";
		cell.appendChild(amountInput);
	}

	var saveButton = document.createElement("BUTTON");
	saveButton.style.width = "100px";
	saveButton.type = "submit";
	saveButton.innerHTML = "Save";
	saveButton.onclick = function() {
		var params = JSON.stringify(readData(fishDataTable, playerCardsTable));
		var http = new XMLHttpRequest();
		http.open("POST", url + '/data', true);
		http.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status == 200) {
					alert("Data successfully submitted");
				} else {
					alert("Error while submiting data");
				}
				showLogin();
			}
		};
		http.setRequestHeader("Content-type", "application/json");
		http.send(params);
	};

	content.appendChild(fishDataTable);
	content.appendChild(document.createElement("P"));
	content.appendChild(playerCardsTable);
	content.appendChild(document.createElement("P"));
	content.appendChild(saveButton);
}

function readData(fishDataTable, playerCardsTable) {
	var fishData = [];
	for (var i = 1; i < fishDataTable.rows.length; i++) {
		var row = fishDataTable.rows[i];
		var dataIndex = i - 1;
		fishData[dataIndex] = {
			id:row.cells[0].innerHTML,
			name:row.cells[1].innerHTML,
			amount:row.cells[2].firstChild.value
		};
	}

	var playerData = [];
	for (var i = 1; i < playerCardsTable.rows.length; i++) {
		var row = playerCardsTable.rows[i];
		var dataIndex = i - 1;
		playerData[dataIndex] = {
			id:row.cells[0].innerHTML,
			name:row.cells[1].innerHTML,
			amount:row.cells[2].firstChild.value
		};
	}

	return {fishData, playerData};
}
